package it.uniroma3;



import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.ResponsabileRepository;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@SpringBootApplication 
public class SiwDemoSpringApplication {

	@Autowired
	private CentroService centroService; 
	
	@Autowired
	private AttivitaService attivitaService; 
	
	@Autowired
	private AllievoService allievoService; 
	
	@Autowired
	private ResponsabileRepository responsabileRepository; 
	
	public static void main(String[] args) {
		SpringApplication.run(SiwDemoSpringApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		Centro centro1 = new Centro("Centro Uno", "via dei navigatori 13", "centrouno@email.com","063333444",120);
		Centro centro2 = new Centro("Centro Due", "piazza cavour 12", "centrodue@email.com","068812345",114);
		Centro centro3 = new Centro("Centro Tre", "via garibaldi 42", "centrotre@email.com","068933321",111);

	
		centroService.addCentro(centro1);
		centroService.addCentro(centro2);
		centroService.addCentro(centro3);
		
		Responsabile resp=new Responsabile("Marco","Verdi","admin_mverdi","mverdi",centro1);
		Responsabile resp2=new Responsabile("Giulio","Marchi","admin_gmarchi","gmarchi",centro3);

		
		responsabileRepository.save(resp);
		responsabileRepository.save(resp2);

		
		
	
		Attivita att=new Attivita("Corso su database","21 gennaio","12:00");
		Attivita att2=new Attivita("Corso su web marketing","13 marzo","11:30");
		att.setCentro(centro1);
		att2.setCentro(centro1);
		
		attivitaService.addAttivita(att);
		attivitaService.addAttivita(att2);

		
		Allievo allievo=new Allievo("Mario","Rossi","m.rossi@gmail.com","","15/05/1988");
		allievo.getAttivita().add(att);
		allievo.getCentri().add(centro1);
		
		allievoService.addAllievo(allievo);
		
		Allievo allievo2=new Allievo("Giulio","Neri","g_neri@hotmail.it","3332211932","12/02/1990");
		Allievo allievo3=new Allievo("Marco","Bianchi","","3394455650","1/04/1992");
		
		allievo2.getCentri().add(centro2);
		allievo3.getCentri().add(centro3);


		allievoService.addAllievo(allievo2);
		allievoService.addAllievo(allievo3);

		
		
	
	
	}
}
