package it.uniroma3.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import it.uniroma3.controller.validator.AllievoValidator;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;

import it.uniroma3.repository.AllievoRepository;

import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AllievoController {
	
	
    
    @Autowired
    private AllievoService allievoService;
    
    
    @Autowired
    private AttivitaService attivitaService;
    
    @Autowired
    private AllievoRepository allievoRepository;

    
    @Autowired
    private AllievoValidator allievoValidator;
    
    @Autowired
    private CentroService centroService;


    @GetMapping("/allAllievi")
    public String AllAllieviAzienda( Model model) {

	
    	model.addAttribute("allievi",this.allievoService.findAll());
    	return "ListaAllievi";
    }
   

    
    @GetMapping("/allAllievi/{attivitaId}")
    public String AllAllievi(@PathVariable("attivitaId") Long id, Model model) {
    	Attivita attivita=this.attivitaService.findById(id);

	Collection<Allievo> allievi=  (Collection<Allievo>) this.allievoRepository.findByCentriId(attivita.getCentro().getId());
    allievi.removeAll(attivita.getAllievi());
        
    	model.addAttribute("allievi",allievi);
        model.addAttribute("attivita",attivita);
    	return "AddAllievi";
    }
   
   
    
	   
	   @GetMapping("/allievi/{attivitaId}")
	    public String getAllieviOfAttivita(@PathVariable("attivitaId") Long id, Model model) {

		   
		   
	        model.addAttribute("allievi", this.allievoRepository.findByAttivitaId(id));
	        model.addAttribute("attivita",this.attivitaService.findById(id));
	    	return "ListaAllieviAttivita";
	    }
	   
	   
	   
	   @GetMapping("/addAllievo/{centroId}")
	   public String addAllievo(@PathVariable("centroId") Long id, Model model) {
		  
		 
	        model.addAttribute("allievo",new Allievo());
	        model.addAttribute("centro",this.centroService.findById(id));
	     
	    	return "allievoForm";
	    }


	 
	   @GetMapping("/addAllievo/{attivitaId}/{allievoId}")
	    public String addAttivita(@PathVariable("attivitaId") Long id,@PathVariable("allievoId") Long id2, Model model) {
		   
		   this.allievoService.findById(id2).getAttivita().add(this.attivitaService.findById(id));
		  model.addAttribute("allievi", this.allievoRepository.findByAttivitaId(id));
	        model.addAttribute("attivita",this.attivitaService.findById(id));
	     
	    	return this.getAllieviOfAttivita(id, model);
	    }

	   @PostMapping("/saveAllievoToCentro/{centroId}")
	    public String newAllievoToCentro(@Valid @ModelAttribute("allievo") Allievo allievo,@PathVariable("centroId") Long id,
	    									Model model, BindingResult bindingResult) {
	    	
	    
	       this.allievoValidator.validate(allievo, bindingResult);
	   
	      if (this.allievoService.alreadyExists(allievo)) {
	            model.addAttribute("exists", "Attivita already exists");
	        }
	        else {
	            if (!bindingResult.hasErrors()) {
	            	allievo.getCentri().add(this.centroService.findById(id));
	                this.allievoService.addAllievo(allievo);

	                return "redirect:/";
	            }
	        }
	       

	        return "allievoForm";
	    }
	    
	   
	   @GetMapping("/deleteAllievo/{allievoId}/{attivitaId}")
	    public String deleteAttivita(@PathVariable ("allievoId") Long id,@PathVariable ("attivitaId") Long id2,Model model) {
	 
		// this.attivitaService.findById(id2).getAllievi().remove(this.allievoService.findById(id));
this.allievoService.findById(id).getAttivita().remove(this.attivitaService.findById(id2));		   
		 model.addAttribute("allievi", this.allievoRepository.findByAttivitaId(id));
	        model.addAttribute("attivita",this.attivitaService.findById(id2));
	    	return "ListaAllievi";      
	    } 
	 
	   
	   
	    @PostMapping("/saveAllievo/{id}")
	    public String newAttivita(@Valid @ModelAttribute("allievo") Allievo allievo, @PathVariable("id") Long id,
	    									Model model, BindingResult bindingResult) {
	    	
	    
	       this.allievoValidator.validate(allievo, bindingResult);
	   
	     /*   if (this.attivitaService.alreadyExists(allievo)) {
	            model.addAttribute("exists", "Attivita already exists");
	        }
	        else {*/
	            if (!bindingResult.hasErrors()) {
	    			allievo.getAttivita().add(this.attivitaService.findById(id));
	                this.allievoService.addAllievo(allievo);

	                return this.getAllieviOfAttivita(id, model);
	            }
	      //  }
	       
	        model.addAttribute("attivita",this.attivitaService.findById(id));

	        return "allievoForm";
	    }
	    
	
    
}

