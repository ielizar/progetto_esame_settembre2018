package it.uniroma3.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import it.uniroma3.controller.validator.CentroValidator;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.repository.ResponsabileRepository;
import it.uniroma3.service.CentroService;

@Controller
public class CentroController {
	
    @Autowired
    private CentroService centroService;
    
    

 @Autowired
 private ResponsabileRepository responsabileRepository;

 
    @Autowired
    private CentroValidator centroValidator;
 
    
    @RequestMapping("/login")
    public String login(Model model,String error,String logout) {
    	if(error != null) {
    		
    		model.addAttribute("errorMsg","Your username and password are invalid");
    		
    	}
    	
    
    	
		return "login";
    	
    	
    	
    }

    @GetMapping("/findOne")
    @ResponseBody
    public Centro findOne(Long id) {
    	Centro c=this.centroService.findById(id);
    	c.setAttivita(new ArrayList<>());
    	
return c;     }

 
    
    @PostMapping("/save")
    public String saveCentro(Centro centro,Model model) {
this.centroService.addCentro(centro);
model.addAttribute("centri", this.centroService.findAll());
return "ListaCentri";
    } 
    
    
    @RequestMapping("/allieviCentro/{centroId}")
    public String getAllieviCentro(@PathVariable("centroId") Long id,Model model) {
    	model.addAttribute("centro",this.centroService.findById(id));
        model.addAttribute("allievi", this.centroService.findById(id).getAllievi());
        return "ListaAllievi";
    }

    @RequestMapping("/responsabileCentro")
    public String getCentriResponsabile(Model model) {
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	System.out.println(auth.getName());
    	
    	System.out.println(responsabileRepository.findByUsername(auth.getName()).getCentro().getName());

    
    	Centro c=responsabileRepository.findByUsername(auth.getName()).getCentro();
    	
    	List<Centro> centri=new ArrayList<>();
    	centri.add(c);
    
        model.addAttribute("centri", c);

    	
        return "ListaCentri";
    }

    
    
    @RequestMapping("/centri")
    public String getCentri(Model model) {
    	
        model.addAttribute("centri", this.centroService.findAll());
        return "ListaCentri";
    }


    @RequestMapping("/addCentro")
    public String addCustomer(Model model) {
        model.addAttribute("centro", new Centro());
        return "centroForm";
    }
    
    
    @GetMapping("/delete")
    public String deleteCentro(Long id,Model model) {
    	 for(Attivita a:this.centroService.findById(id).getAttivita()) {
    		
    		a.setCentro(null); 
    		

     }

this.centroService.delete(id);
return "redirect:/";
    } 
   
 

    
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
    

    
    @PostMapping("/saveCentro")
    public String newCentro(@Valid @ModelAttribute("centro") Centro centro,
			Model model, BindingResult bindingResult) {
		
    
     this.centroValidator.validate(centro, bindingResult); 
      
        if (this.centroService.alreadyExists(centro)) {
            model.addAttribute("exists", "Centro already exists");
            return "centroForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.centroService.addCentro(centro);
                model.addAttribute("centri", this.centroService.findAll());
                return this.getCentri(model);
            } 
        } 
        return "centroForm"; 
    } 

}
