package it.uniroma3.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;

import it.uniroma3.repository.AttivitaRepository;

import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AttivitaController {
	
	
    
    @Autowired
    private AttivitaService attivitaService;
    
    
    @Autowired
    private CentroService centroService;
    
    @Autowired
    private AttivitaRepository attivitaRepository;

    
    @Autowired
    private AttivitaValidator attivitaValidator;
	
	   @GetMapping("/attivita/{centroId}")
	    public String getAttivita(@PathVariable("centroId") Long id, Model model) {
	        model.addAttribute("activities", this.attivitaRepository.findByCentroId(id));
	        model.addAttribute("centro",this.centroService.findById(id));
	    	return "ListaAttivita";
	    }
	   
	   @GetMapping("/addAttivita/{centroId}")
	    public String addAttivita(@PathVariable("centroId") Long id, Model model) {
	        model.addAttribute("attivita", new Attivita());
	        model.addAttribute("centro",this.centroService.findById(id));
	    	return "attivitaForm";
	    }
	   
	   
	@GetMapping("/findOneAttivita/{id}")
    @ResponseBody
    public Attivita findOneAttivita(@PathVariable("id") Long id) {
		Attivita a=this.attivitaService.findById(id);
		a.getCentro().setAttivita(new ArrayList<>());
		a.setAllievi(new ArrayList<>());
    	return a;
     }
	   

	   @GetMapping("/deleteAttivita/{id}")
    public String deleteAttivita(@PathVariable ("id") Long id,Model model) {
 
		   for(Allievo a : this.attivitaService.findById(id).getAllievi()) {
			   a.getAttivita().remove(this.attivitaService.findById(id));
			   
		   }
	Long centerId=this.attivitaService.findById(id).getCentro().getId();
this.attivitaService.delete(id);
return this.getAttivita(centerId, model);       
    } 
 
	   
	   
	   @PostMapping("/saveAttivita")
	    public String saveAttivita(Attivita attivita,Model model) {
		//  System.out.println(attivita.getCentro());
	this.attivitaService.addAttivita(attivita);
	return "redirect:/";       
	    } 
	    
	    
	
	   
	   @PostMapping("/saveAttivita/{centroId}")
	    public String saveAttivita(@Valid @ModelAttribute("attivita") Attivita attivita,
	    		@PathVariable("centroId") Long id, Model model,BindingResult bindingResult) {
	       this.attivitaValidator.validate(attivita, bindingResult);

	        if (this.attivitaService.alreadyExists(attivita)) {
	            model.addAttribute("exists", "Attivita already exists");
	        }
	       
	        else {
	            if (!bindingResult.hasErrors()) {
	    			attivita.setCentro(this.centroService.findById(id));
	                this.attivitaService.addAttivita(attivita);

	                return this.getAttivita(id, model);
	            
	        }
	        }
	        model.addAttribute("centro",this.centroService.findById(id));

	    	return "attivitaForm";
	    }
	

	 
    
}

