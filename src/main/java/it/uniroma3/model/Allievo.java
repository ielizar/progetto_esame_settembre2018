package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Allievo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String name;

	@Column(nullable=false)
	private String surname;
	
	@Column
	private String email;
	
	@Column
	private String phone;
	
	
	@Column(nullable=false)
	private String dateOfBirth;
	
	
	@ManyToMany 
	private List<Attivita> attivita=new ArrayList<>();
	
	@ManyToMany 
	private List<Centro> centri=new ArrayList<>();

	public Allievo() {}

	

	public Allievo(String name, String surname, String email, String phone, String dateOfBirth) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.phone = phone;
		this.dateOfBirth = dateOfBirth;
	}



	public Long getId() {
		return id;
	}






	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}



	public List<Attivita> getAttivita() {
		return attivita;
	}



	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}



	public List<Centro> getCentri() {
		return centri;
	}



	public void setCentri(List<Centro> centri) {
		this.centri = centri;
	}



	
}
