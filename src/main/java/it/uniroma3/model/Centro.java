package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String name;

	@Column(nullable=false)
	private String address;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private String phone;
	
	@Column(nullable=false)
	private int NumMaxAllievi;
	
	@OneToMany (mappedBy = "centro",fetch = FetchType.EAGER)
	private List<Attivita> attivita; 

	@ManyToMany (mappedBy="centri")
	private List<Allievo> allievi=new ArrayList<>();
	
	@OneToOne
	private Responsabile responsabile;
	
	
	public Centro() {}

	public Centro(String name, String address,String email,String phone,int NumMaxAllievi) {
	
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.NumMaxAllievi = NumMaxAllievi;
		this.attivita=new ArrayList<>();

			
	}

	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id=id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getNumMaxAllievi() {
		return NumMaxAllievi;
	}

	public void setNumMaxAllievi(int numMaxAllievi) {
		NumMaxAllievi = numMaxAllievi;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}

	

	
}
