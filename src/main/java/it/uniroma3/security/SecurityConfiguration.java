package it.uniroma3.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.ResponsabileRepository;



@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	
	@Autowired
	private ResponsabileRepository responsabileRepository; 

	
	@SuppressWarnings("deprecation")
	@Bean
	public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
		
		List<UserDetails> users=new ArrayList<>();
		
		for(Responsabile r: responsabileRepository.findAll()) {
		users.add(User.withDefaultPasswordEncoder().username(r.getUsername()).password(r.getPassword()).roles("ADMIN_CENTER").build());
			
		}
		
		users.add(User.withDefaultPasswordEncoder().username("root").password("root").roles("ADMIN").build());
		
		return new InMemoryUserDetailsManager(users);

			
		
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and()
		.formLogin().loginPage("/login").permitAll()
		.and().logout()
        .logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login"); 
			http.csrf().disable();
		
		
	}
	

}
