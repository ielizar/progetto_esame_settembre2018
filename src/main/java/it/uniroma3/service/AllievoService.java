package it.uniroma3.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Allievo;
import it.uniroma3.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {
	
	@Autowired
	private AllievoRepository allievoRepository; 
	

	

	public List<Allievo> findAll() {
		return (List<Allievo>) this.allievoRepository.findAll();
	}
	
	public Allievo findById(Long id) {
		Optional<Allievo> customer = this.allievoRepository.findById(id);
		if (customer.isPresent()) 
			return customer.get();
		else
			return null;
	} 
	

	public void addAllievo(Allievo allievo) {
		 this.allievoRepository.save(allievo);
		
	}
	
	public void update(Allievo allievo) {
		 this.allievoRepository.save(allievo);
		
	}
	
	public void delete(Long id) {
		this.allievoRepository.deleteById(id);
		
	}

	public boolean alreadyExists(Allievo all) {
		List<Allievo> customers = this.allievoRepository.findByNameAndSurnameAndDateOfBirth(all.getName(), all.getSurname(),all.getDateOfBirth());
		if (customers.size() > 0)
			return true;
		else 
			return false;
	}	

}
