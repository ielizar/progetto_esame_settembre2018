package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import it.uniroma3.model.Attivita;
import it.uniroma3.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {
	
	@Autowired
	private AttivitaRepository attivitaRepository; 
	

	public List<Attivita> findAll() {
		return (List<Attivita>) this.attivitaRepository.findAll();
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> customer = this.attivitaRepository.findById(id);
		if (customer.isPresent()) 
			return customer.get();
		else
			return null;
	}

	

	public void addAttivita(Attivita attivita) {
		 this.attivitaRepository.save(attivita);
		
	}
	
	public void update(Attivita attivita) {
		 this.attivitaRepository.save(attivita);
		
	}
	
	public void delete(Long id) {
		this.attivitaRepository.deleteById(id);
		
	}
	
	public boolean alreadyExists(Attivita a) {
		List<Attivita> customers = this.attivitaRepository.findByName(a.getName());
		if (customers.size() > 0)
			return true;
		else 
			return false;
	}	
	
}
