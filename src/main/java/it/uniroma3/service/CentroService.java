package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Centro;
import it.uniroma3.repository.CentroRepository;

@Transactional
@Service
public class CentroService {
	
	@Autowired
	private CentroRepository centroRepository; 
	
	
	public List<Centro> findAll() {
		return (List<Centro>) this.centroRepository.findAllByOrderById();
	}
	
	public Centro findById(Long id) {
		Optional<Centro> customer = this.centroRepository.findById(id);
		if (customer.isPresent()) 
			return customer.get();
		else
			return null;
	}

	

	public void addCentro(Centro centro) {
		 this.centroRepository.save(centro);
		
	}
	

	public void delete(Long id) {
		this.centroRepository.deleteById(id);
		
	}
	
	public boolean alreadyExists(Centro centro) {
		List<Centro> centri = this.centroRepository.findByName(centro.getName());
		if (centri.size() > 0)
			return true;
		else 
			return false;
	}	

}
