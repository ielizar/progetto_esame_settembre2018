package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.model.Centro;

@Repository
public interface CentroRepository extends CrudRepository<Centro, Long> {


public List<Centro> findByName(String name);


public List<Centro> findAllByOrderById();

	
	
}
