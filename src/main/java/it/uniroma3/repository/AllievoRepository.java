package it.uniroma3.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {

	List<Allievo> findByNameAndSurnameAndDateOfBirth(String name, String surname, String dateOfBirth);
	
	List<Allievo> findByAttivitaId(Long id);
	
	List<Allievo> findByCentriId(Long id);


	
}
